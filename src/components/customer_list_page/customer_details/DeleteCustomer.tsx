import React from "react";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DeleteIcon from "@material-ui/icons/DeleteOutline";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";

import { makeStyles } from "@material-ui/core/styles";

import Customer from "../types";


const useStyles=makeStyles({
  blueButton: {
    backgroundColor:"blue",
    color:"white",
    "&:hover":{
      color:"black"
    }
  },
  icons: {
    margin: "0% 1%",
  },
});

interface Props{
  closeDrawer: () => void;
  deleteCustomer:(id: number) => void;
  customer:Customer;
}

const DeleteCustomer:React.FC<Props>=({closeDrawer,deleteCustomer,customer})=>{
  const styles=useStyles();

  //state for deleting a customer
  const [open,setOpen]=React.useState(false);
  const handleOpen=()=>{
    setOpen(true);
  };
  const handleClose=(val:boolean)=>{
    if(val){
      closeDrawer();
      deleteCustomer(customer.id);
    }
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Tooltip title="Delete customer">
      <DeleteIcon className={styles.icons} onClick={() => { handleOpen(); }}></DeleteIcon>
      </Tooltip>
        <Dialog
        open={open}
        onClose={()=>{handleClose(false);}}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Delete customer</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure you want to continue?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>{handleClose(false)}}>CANCEL</Button>
            <Button onClick={()=>{handleClose(true)}} className={styles.blueButton}>CONFIRM</Button>
          </DialogActions>
        </Dialog>
        

    </React.Fragment>
  );
}


export default DeleteCustomer;