import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import CreateIcon from "@material-ui/icons/CreateOutlined";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import ClearIcon from "@material-ui/icons/Clear";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import MuiPhoneNumber from "material-ui-phone-number";
import Tooltip from "@material-ui/core/Tooltip";

const usestyles=makeStyles({
  blueButton: {
    backgroundColor:"blue",
    color:"white",
    "&:hover":{
      color:"black"
    }
  },
  alignEnd:{
    textAlign:"end",
  },
  icons: {
    margin: "0% 1%",
  },
})

interface Props{
  editCustomer: (index: number, path: string, newVal: string | number) => void;
  index:number;
  closeDrawer: () => void;
}

const EditCustomer:React.FC<Props>=({editCustomer,index,closeDrawer})=>{
  const styles=usestyles();
  
  const [name,setName]=React.useState("");
  const handleChangeName=(event:React.ChangeEvent<HTMLInputElement>)=>{
    setName(event.target.value);
  }

  const [email,setEmail]=React.useState("");
  const handleChangeEmail=(event:React.ChangeEvent<HTMLInputElement>)=>{
    setEmail(event.target.value);
  }

  const [telephone,setTelephone]=React.useState("");
  const handleChangeTelephone=(val:string)=>{
    setTelephone(val);
  }
   //state for updating a customer
   const [openUpdate,setOpenUpdate]=React.useState(false);
   const handleOpenUpdate=()=>{
     setOpenUpdate(true);
   }
   const handleCloseUpdate=(val:boolean)=>{
     if(val){
       editCustomer(index,"name",name);
       editCustomer(index,"mail",email);
       editCustomer(index,"details.telephone",telephone);
       closeDrawer();//closing drawer after editing
       console.log(name+email+telephone);
     }
     setOpenUpdate(false);
   };

  return(
    <React.Fragment>
      <Tooltip title="Edit customer">
      <CreateIcon className={styles.icons} onClick={() => { handleOpenUpdate();}}></CreateIcon>
      </Tooltip>
  <Dialog
  open={openUpdate}
  onClose={()=>{handleCloseUpdate(false);}}
  aria-labelledby="alert-dialog-title"
  aria-describedby="alert-dialog-description"
  >
    <DialogTitle>
    <div className={styles.alignEnd}><ClearIcon onClick={()=>{handleCloseUpdate(false);}}></ClearIcon></div>
      Update customer

      
    </DialogTitle>
    <DialogContent>
      <TextField
      autoFocus
      id="name"
      label="Name"
      margin="dense"
      type="string"
      fullWidth
      onChange={handleChangeName}
      />
      <TextField
      autoFocus
      id="email"
      label="Email Address"
      margin="dense"
      type="email"
      fullWidth
      onChange={handleChangeEmail}
      />
      <MuiPhoneNumber
        name="phone"
        label="phone"
        defaultCountry={"us"}
        onChange={handleChangeTelephone}
        fullWidth
        />
    </DialogContent>
    <br/>
    <DialogActions>
      <Button onClick={()=>{handleCloseUpdate(false)}}>CANCEL</Button>
      <Button onClick={()=>{handleCloseUpdate(true)}} className={styles.blueButton}>SAVE</Button>
    </DialogActions>
  </Dialog>
    </React.Fragment>
);
}


export default EditCustomer;