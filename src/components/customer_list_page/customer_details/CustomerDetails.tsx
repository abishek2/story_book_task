import React from "react";
import Drawer from "@material-ui/core/Drawer";
import Card from "@material-ui/core/Card";
import Avatar from "@material-ui/core/Avatar";
import CardHeader from '@material-ui/core/CardHeader';
import { makeStyles } from "@material-ui/core/styles";
import ClearIcon from "@material-ui/icons/Clear";
import Tooltip from "@material-ui/core/Tooltip";


import Customer from "../types";
import EditCustomer from "./EditCustomer";
import DeleteCustomer from "./DeleteCustomer";


const useStyles = makeStyles({
  customListCard: {
    marginTop: "10px"
  },
  optionMenu: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    width: "100%"
  },
  icons: {
    margin: "0% 1%",
  },
  drawerPaper: {
    width: "80%",
  },
  drawerStyle: {
    width: "80%",
  },
  keyValueSpace: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    margin: "2% 1%",
  },
  keySpace: {
    width: "30%"
  },
  valueSpace: {
    width: "70%"
  },
  drawerItemSpace: {
    margin: "5%",
  },
  keyVlaueCard: {
    marginTop: "2%",
  },
  lightBlackColor: {
    color: "#6e6b6b"
  },
  buttonSection: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },

});

interface Props {
  drawerState: boolean;
  customer: Customer;
  index: number;
  closeDrawer: () => void;
  deleteCustomer: (id: number) => void;
  editCustomer: (index: number, path: string, newVal: string | number) => void;
}

const CustomerDetails: React.FC<Props> = ({ drawerState, customer, index, closeDrawer, deleteCustomer, editCustomer }) => {

  const styles = useStyles();

  const KeyValue = (key: string, val: string) => {
    return (<div className={styles.keyValueSpace} >
      <div className={styles.lightBlackColor}>{key}</div>
      <span className={styles.valueSpace}>{val}</span>
    </div>);
  }

  return (
    customer ? (<Drawer
      className={styles.drawerStyle}
      anchor="right"
      classes={
        {
          paper: styles.drawerPaper,
        }
      }
      open={drawerState}
      onClose={closeDrawer}
    >
      <section className={`${styles.optionMenu} ${styles.lightBlackColor}`} >
          <EditCustomer editCustomer={editCustomer} index={index} closeDrawer={closeDrawer} />
          <DeleteCustomer deleteCustomer={deleteCustomer} closeDrawer={closeDrawer} customer={customer} />
        <Tooltip title="close customer">
          <ClearIcon className={styles.icons} onClick={() => { closeDrawer(); }}></ClearIcon>
        </Tooltip>
      </section>

      <section className={styles.customListCard}>
        <CardHeader
          avatar={<Avatar>{customer.name.charAt(0).toUpperCase()}</Avatar>}
          title={customer.name}
          subheader={`${customer.mail}`}
        ></CardHeader>
      </section>

      <section className={`${styles.drawerItemSpace} ${styles.lightBlackColor}`}>
        <div>Details</div>
        <Card className={styles.keyVlaueCard}>
          {KeyValue("company", customer.details.company)}
          {KeyValue("Address", customer.details.address)}
          {KeyValue("Telephone", customer.details.telephone)}
          {KeyValue("Language", customer.details.language)}
          {KeyValue("Timezone", customer.details.timezone)}
        </Card>
      </section>

      <section className={`${styles.drawerItemSpace} ${styles.lightBlackColor}`}>
        <div>Special Dates</div>
        <Card>
          {KeyValue("Birth Date", customer.specialDates.birthDate)}
        </Card>
      </section>

    </Drawer>) : (<></>)
  );
}

export default CustomerDetails;