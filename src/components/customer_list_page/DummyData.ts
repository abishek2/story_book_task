import Customer from "./types";
const initCustomersList:Customer[]=[
  {
    id:1,
    name:"Abishek",
    mail:"abishek@example.com",
    image:1,
    details:{
      company:"ATP",
      address:"1234,The wall street,America,Earth",
      language:"english",
      telephone:"9876543210",
      timezone:"Mountain Time Zone",
    },
    specialDates:{
      birthDate:"01/01/2022",
    }
  },
  {
    id:2,
    name:"Raj",
    mail:"raj@example.com",
    image:1,
    details:{
      company:"ATP",
      address:"1234,The wall street,America,Earth",
      language:"english",
      telephone:"9876543210",
      timezone:"Mountain Time Zone",
    },
    specialDates:{
      birthDate:"01/01/2022",
    }
  },
  {
    id:3,
    name:"Dinesh",
    mail:"dinesh@example.com",
    image:1,
    details:{
      company:"ATP",
      address:"1234,The wall street,America,Earth",
      language:"english",
      telephone:"9876543210",
      timezone:"Mountain Time Zone",
    },
    specialDates:{
      birthDate:"01/01/2022",
    }
  },
  {
    id:4,
    name:"Ram",
    mail:"ram@example.com",
    image:1,
    details:{
      company:"ATP",
      address:"1234,The wall street,America,Earth",
      language:"english",
      telephone:"9876543210",
      timezone:"Mountain Time Zone",
    },
    specialDates:{
      birthDate:"01/01/2022",
    }
  },
  {
    id:5,
    name:"Raja",
    mail:"raja@example.com",
    image:1,
    details:{
      company:"ATP",
      address:"1234,The wall street,America,Earth",
      language:"english",
      telephone:"9876543210",
      timezone:"Mountain Time Zone",
    },
    specialDates:{
      birthDate:"01/01/2022",
    }
  },
];


export default initCustomersList;