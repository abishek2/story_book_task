import React from "react";
import {makeStyles} from "@material-ui/core/styles";


import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import ClearIcon from "@material-ui/icons/Clear";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import MuiPhoneNumber from "material-ui-phone-number";



const useStyles=makeStyles({
  alignEnd:{
    textAlign:"end",
  },
  blueButton: {
    backgroundColor:"blue",
    color:"white",
    "&:hover":{
      color:"black"
    }
  },
})

interface Props{
  handleCloseAdd: (val: boolean, cusName: string, email: string, telephone: string) => void;
  openAdd:boolean;
}

const AddCustomer:React.FC<Props>=({openAdd,handleCloseAdd,})=>{

  const [cusName,setName]=React.useState<string>("");
  const handleChangeName=(event:React.ChangeEvent<HTMLInputElement>)=>{
    setName(event.target.value);
  }

  const [email,setEmail]=React.useState<string>("");
  const handleChangeEmail=(event:React.ChangeEvent<HTMLInputElement>)=>{
    setEmail(event.target.value);
  }

  const [telephone,setTelephone]=React.useState<string>("");
  const handleChangeTelephone=(val:string)=>{
    setTelephone(val);
  }

  const styles=useStyles();

  return (<Dialog
    open={openAdd}
    onClose={()=>{handleCloseAdd(false,cusName,email,telephone);}}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    >
      <DialogTitle>
      <div className={styles.alignEnd}><ClearIcon onClick={()=>{handleCloseAdd(false,cusName,email,telephone);}}></ClearIcon></div>
        Update customer

        
      </DialogTitle>
      <DialogContent>
        <TextField
        autoFocus
        id="name"
        label="Name"
        margin="dense"
        type="string"
        fullWidth
        onChange={handleChangeName}
        />
        <TextField
        autoFocus
        id="email"
        label="Email Address"
        margin="dense"
        type="email"
        fullWidth
        onChange={handleChangeEmail}
        />
        <MuiPhoneNumber
        name="phone"
        label="phone"
        defaultCountry={"us"}
        onChange={handleChangeTelephone}
        fullWidth
        />
      </DialogContent>
      <br/>
      <DialogActions>
        <Button onClick={()=>{handleCloseAdd(false,cusName,email,telephone)}}>CANCEL</Button>
        <Button onClick={()=>{handleCloseAdd(true,cusName,email,telephone)}} className={styles.blueButton}>SAVE</Button>
      </DialogActions>
    </Dialog>
);
}

export default AddCustomer;