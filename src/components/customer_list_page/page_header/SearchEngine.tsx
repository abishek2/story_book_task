import Customer from "../types";

const SearchEngine=(searchedKey:string,customers:Customer[],addSearchResult:(newSearchResult:Customer[])=>void,toggleIsSearchEmpty:(val:boolean)=>void,):void=>{
  if(searchedKey==="undefined"||""){
    toggleIsSearchEmpty(true);
    return;
  }
  toggleIsSearchEmpty(false);
  addSearchResult(customers.filter((customer)=>customer.name.toUpperCase().indexOf(searchedKey.toUpperCase())!==-1));/*converting everything to uppercase to do case insensitive comparison.checks is searcheKey part of name that's why we use indexOf() method*/
}

export default SearchEngine;