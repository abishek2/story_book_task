import React from "react";
import {makeStyles} from "@material-ui/core/styles";


import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from '@material-ui/icons/Add';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import Tooltip from "@material-ui/core/Tooltip";

import AddCustomer from "./AddCustomer";
import Customer from "../types";
import SearchEngine from "./SearchEngine";

const useStyles=makeStyles({
  header:{
    display:"flex",
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center",
    width:"100%",
    padding:"0% 2%",
  },
  searchBar:{
    borderRadius:"8%",
    borderColor:"red",
    borderStyle:"solid",
  },
  actions:{
    display:"flex",
    justifyContent:"flex-end",
    width:"70%",
    margin:"0% 1%",
  },
  myTitle:{
    borderRight:"2%",
  },
  addIcon:{
    backgroundColor:"#ebeced",
    margin:"1% 0 1% 2%",
  },
});

interface Props{
  handleOpenAdd: () => void;
  handleCloseAdd: (val: boolean, cusName: string, email: string, telephone: string) => void;
  openAdd:boolean;
  addSearchResult:(newSearchResult:Customer[])=>void;
  customers:Customer[];
  toggleIsSearchEmpty:(val:boolean)=>void;
}

const PageHeader:React.FC<Props>=({handleOpenAdd,handleCloseAdd,openAdd,addSearchResult,customers,toggleIsSearchEmpty})=>{

  const getSearchResult=(searchedKey:React.ChangeEvent<HTMLInputElement>)=>{
    SearchEngine(searchedKey.target.value,customers,addSearchResult,toggleIsSearchEmpty);
  }
  
  const styles=useStyles();
  return ( <div className={styles.header}>
    <h2 className={styles.myTitle}>Customers</h2>
    <div className={styles.actions}>
      <TextField 
      type="string"
      placeholder="Type to search"
      variant="outlined"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchOutlinedIcon />
          </InputAdornment>
        ),
      }}
      onChange={getSearchResult}
      className={styles.searchBar}
      />
      <Tooltip title="Add customer">
      <IconButton className={styles.addIcon} onClick={()=>{
        handleOpenAdd();
      }}><AddIcon/></IconButton>
      </Tooltip>
    </div>
    <AddCustomer openAdd={openAdd} handleCloseAdd={handleCloseAdd}/>
  </div>);
}

export default PageHeader;