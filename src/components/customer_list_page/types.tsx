interface Customer{
  id:number;
  name:string;
  mail:string;
  image:number;//yet to change in into image path
  details:{
    company:string;
    address:string;
    telephone:string;
    language:string;
    timezone:string
  };
  specialDates:{
    birthDate:string;
  };
};
export default Customer;