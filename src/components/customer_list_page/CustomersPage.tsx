import React from "react";
import remove from "lodash/remove";
import update from "lodash/update";
import {makeStyles} from "@material-ui/core/styles";

import Snackbar from "@material-ui/core/Snackbar";
import Alert from '@material-ui/lab/Alert';
import Button from "@material-ui/core/Button";

import CustomerList from "./custom_list/CustomerList";
import CustomerDetails from "./customer_details/CustomerDetails";
import Customer from "./types";
import initCustomersList from "./DummyData";
import PageHeader from "./page_header/PageHeader";

const useStyles=makeStyles({
  snackbarStyle:{
    backgroundColor:"#52a8ff",
    color:"white"
  },
  textWhite:{
    color:"white"
  }
})
const CustomersPage:React.FC=()=>{
  const styles=useStyles()
  const [isSearchEmpty,setIsSearchEmpty]=React.useState(true);
  const toggleIsSearchEmpty=(val:boolean)=>{
    setIsSearchEmpty(val);
  }
  const [customerIndex,setCustomerIndex]=React.useState(0);
  const [customers,setCustomers]=React.useState(initCustomersList);
  const [searchResult,setSearchResult]=React.useState<Customer[]>([]);

  const [openAlert,setOpenAlert]=React.useState(false);
  const handleOpenAlert=()=>{
    setOpenAlert(true);
  } 
  const handleCloseAlert=()=>{
    setOpenAlert(false);
  }

  //state to control Adding new customer
  const [openAdd,setOpenAdd]=React.useState(false);
  const handleOpenAdd=()=>{
    setIsSearchEmpty(true);//when i add customer i want to set isSearchEmpty to 0 to correctly add customer
    setOpenAdd(true);
  }
  const handleCloseAdd=(val:boolean,cusName:string,email:string,telephone:string,)=>{
    if(val){
      const newCustomer:Customer={
        id:customers.length>0?customers[customers.length-1].id+1:1,//when customer list is empty then id will start from 1
        name:cusName,
        mail:email,
        image:1,
        details:{
          company:"",
          address:"",
          language:"",
          telephone:telephone,
          timezone:"",
        },
        specialDates:{
          birthDate:"",
        }
      }
      setCustomers([...customers,newCustomer]);
    }
    setOpenAdd(false);
  }

  const [drawerState,setDrawerState]=React.useState(false);

  const openCustomerDetailsPage=(index:number)=>{
    setCustomerIndex(index);
    setDrawerState(true);
  };
  
  const closeDrawer=()=>{
    setDrawerState(false);
  };

  const deleteCustomer=(id:number)=>{
    remove(customers,(customer:Customer)=>{
      return id===customer.id;
    });
    console.log(customers);
    setCustomers(customers);
    handleOpenAlert();/*opening alert when item in list is deleted*/
  };

  //this method adds searched result to the search result array
  const addSearchResult=(newSearchResult:Customer[])=>{
    setSearchResult([...newSearchResult]);
  }


  const editCustomer=(index:number,path:string,newVal:string|number)=>{
    update(customers[index],path,()=>newVal);
  }

  return (<main>
    <PageHeader openAdd={openAdd} handleOpenAdd={handleOpenAdd} handleCloseAdd={handleCloseAdd} addSearchResult={addSearchResult} customers={customers} toggleIsSearchEmpty={toggleIsSearchEmpty}/>

    <CustomerList customerList={isSearchEmpty?customers:[]} openCustomerDetailsPage={openCustomerDetailsPage}/>
    <CustomerList customerList={isSearchEmpty?[]:searchResult} openCustomerDetailsPage={openCustomerDetailsPage}/>

    <CustomerDetails index={customerIndex} deleteCustomer={deleteCustomer} 
    editCustomer={editCustomer} drawerState={drawerState} customer={customers[customerIndex]} closeDrawer={closeDrawer}/>
    
    <Snackbar
        open={openAlert}
        autoHideDuration={5000}
        onClose={()=>{handleCloseAlert();}}
        >
          <Alert className={styles.snackbarStyle} onClose={()=>{handleCloseAlert();}} severity="info" action={
          <Button className={styles.textWhite} >DISMISS</Button>
        }>Customer deleted successfully</Alert>
          
        </Snackbar>
  </main>);
}

export default CustomersPage;