import React from "react";
import {makeStyles} from "@material-ui/core/styles"
import Card from "@material-ui/core/Card";
import List from "@material-ui/core/List";
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from "@material-ui/core/Avatar";

import Customer from "../types";

const useStyles=makeStyles({
  customListCard:{
    marginTop:"10px"
  }
});

interface Props{
  customerList:Customer[];
  openCustomerDetailsPage:(index:number)=>void;
}

const CustomerList:React.FC<Props>= ({customerList,openCustomerDetailsPage})=>{
  const styles=useStyles();
  return (<List>
    {customerList.map((value,index,arr)=>{
      return (typeof value !== "undefined")&&(
        <Card key={value.id} onClick={()=>{openCustomerDetailsPage(index);}} className={styles.customListCard}>
          <CardHeader
          avatar={<Avatar>{value.name.charAt(0).toUpperCase()}</Avatar>}
          title={value.name}
          subheader={`${value.mail}`}
          ></CardHeader>
        </Card>
      );
    })}
  </List>);
}

export default CustomerList;