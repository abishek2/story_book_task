import React from "react";
import StopWatchHook from "./Stopwatch_h";

const data={
  title:"Stop watch hook",
  component:StopWatchHook,
};
export default data;

export const stopWatchHook= ()=>{
  return <StopWatchHook/>;
};