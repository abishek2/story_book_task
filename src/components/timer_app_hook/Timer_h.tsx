import React from "react";
import "./Timer_h.css"

let isFirstTime: boolean = true;
let timerId:NodeJS.Timeout;
const TimerHook: React.FC = () => {
  const [timerVal, setTimerValue] = React.useState<number[]>([0, 0, 0, 0]);
  let timerCount: number=86407;
  
  React.useEffect(() => {
    if (isFirstTime) {
      timerId=setInterval(formatTime, 1000);
      isFirstTime=false;
    }
  })

  const formatTime = () => {
    timerCount--; 
    console.log(timerCount);
    let tempTimerVal = timerCount;
    let newTime: number[] = [0, 0, 0, 0];
    if (tempTimerVal <= 0) {
      tempTimerVal = 86407;
    }
    if (tempTimerVal / 86400 >= 1) {
      newTime[0] = Math.floor(tempTimerVal / 86400);
      tempTimerVal %= 86400;
    }
    if (tempTimerVal / 3600 >= 1) {
      newTime[1] = Math.floor(tempTimerVal / 3600);
      tempTimerVal %= 3600;
    }
    if (tempTimerVal / 60 >= 1) {
      newTime[2] = Math.floor(tempTimerVal / 60);
      tempTimerVal %= 60;
    }
    newTime[3] = tempTimerVal;
    setTimerValue(newTime);
  }

  const clearTimer = () => {
    timerCount = 86410;
    clearInterval(timerId);
    timerId=setInterval(formatTime, 1000);
  }

  return (<main>
    <section className="headerSection">
      <div className="title">Countdown Timer</div>
      <div className="buttonsSection">
        <button onClick={clearTimer} className="button clearButton">Clear</button>
        <button className="button settingsButton">Settings</button>
      </div>
    </section>

    <section className="contentSection">
      <div className="mainContent">
        <div className="titleContent">
          Countdown ends in...
        </div>

        <div className="timeSection">

          <div className={`time time${timerVal[0]}`}>
            <div>{timerVal[0]}</div>
            <div className="label">Days</div>
          </div>

          <div className={`time time${timerVal[1]}`}>
            <div>{timerVal[1]}</div>
            <div className="label">Hours</div>
          </div>

          <div className={`time time${timerVal[2]}`}>
            <div>{timerVal[2]}</div>
            <div className="label">Mins</div>
          </div>

          <div className={`time time${timerVal[3]}`}>
            <div>{timerVal[3]}</div>
            <div className="label">Secs</div>
          </div>

        </div>
      </div>
    </section>

  </main>);
}

export default TimerHook;